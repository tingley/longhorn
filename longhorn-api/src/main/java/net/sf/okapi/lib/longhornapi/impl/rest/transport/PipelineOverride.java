package net.sf.okapi.lib.longhornapi.impl.rest.transport;

import javax.xml.bind.annotation.XmlAttribute;

public abstract class PipelineOverride {
	private String type;

	@XmlAttribute
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
