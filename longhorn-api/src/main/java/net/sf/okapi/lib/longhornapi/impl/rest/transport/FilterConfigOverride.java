package net.sf.okapi.lib.longhornapi.impl.rest.transport;

public class FilterConfigOverride extends PipelineOverride {
	private String fileExtension;
	private String filterName;
	private String filterParams;

	public FilterConfigOverride() {
	}

	/**
	 * @return the file extension for this override
	 */
	public String getFileExtension() {
		return fileExtension;
	}

	/**
	 * @param fileExtension the file extension to override, include the '.'
	 */
	public void setFileExtension(String fileExtension) {
		this.fileExtension = fileExtension;
	}

	/**
	 * @return the filterClassName
	 */
	public String getFilterName() {
		return filterName;
	}

	/**
	 * @param filterClassName the filterClassName to set
	 */
	public void setFilterName(String filterName) {
		this.filterName = filterName;
	}

	/**
	 * @return the FilterParams
	 */
	public String getFilterParams() {
		return filterParams;
	}

	/**
	 * @param FilterParams the FilterParams to set
	 */
	public void setFilterParams(String FilterParams) {
		this.filterParams = FilterParams;
	}

	@Override
	public String getType() {
		return "filterConfig";
	}
}
